* On the client machine:
    * `Preferences > Install` and type `remote-atom`. `Packages > Remote Atom > Start Server`
    * One has always to first open Atom and start the server, otherwise it won't work
* On your server/other machine:
    * `sudo wget -O /usr/local/bin/ratom https://raw.github.com/aurora/rmate/master/rmate`
    * `sudo chmod +x /usr/local/bin/ratom`
    * `ratom your_file.txt` (it will open in Atom locally!)
