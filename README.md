# Install Atom

This repository contains instructions to install Atom text editor [1] in order for it to be used locally to change remote files (`ssh`).

-----
[1] https://atom.io/
